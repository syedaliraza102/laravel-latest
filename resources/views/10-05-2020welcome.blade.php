<style>
.StripeElement {
  box-sizing: border-box;
  width:100%;
  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@if(session()->has('message'))
        
        <div class="alert alert-danger" role="alert">
         Error! {{session()->get('message')}}
        </div>
        @endif
 <form action="{{url('proceed_payments')}}" method="post" id="payment-form">
       {{ csrf_field() }}
        <div class="modal-body">
        <div class="form-row mb-3" >
        <label for="card-element">Email</label>
        <input type="email" class="form-control" name="stripe_email" value="syedaliraza102@gmail.com"  >
        </div>
        
        <div class="form-row mb-3">
          <label for="card-element">
            Credit or debit card
          </label>
          <div id="card-element">
            <!-- A Stripe Element will be inserted here. -->
          </div>
          <input type="hidden" name="strip_amount" value="10" class="strip-amount">
          <input type="hidden" name="strip_planid" value="price_1GqBMyLBTG4noOv2YF3izOl6" class="strip-planid">
          <!-- Used to display form errors. -->
          <div id="card-errors" role="alert"></div>
         </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="proceed-payment">Proceed Payment</button>
        </div>
      </div>
  </form>
    <input  type="hidden" value="{{ env('STRIPE_KEY') }}" id="strip-key"> 
     
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="4AJR73XY963TQ">
<input type="image" src="https://www.paypalobjects.com/en_GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>

  <!-- /page content -->
<script src="https://js.stripe.com/v3/"></script>
<script>
$('.get-started').on('click',function(){
    var amount=$(this).attr('data-amount');
    var pakname=$(this).attr('data-pakname');
    var planid=$(this).attr('data-planid');
    $('#payment-model-title').html('');
    $('#payment-model-title').html(pakname+' ('+amount+')');
    $('.strip-planid').val(planid);
    $('.strip-amount').val(amount);
    $("#payment-model").modal('show')

    
});
var strip_key =$('#strip-key').val();
// Create a Stripe client.
var stripe = Stripe(strip_key);
// Create an instance of Elements.
var elements = stripe.elements();
// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.on('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();
  $('#proceed-payment').prop('disabled', true);
  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
      $('#proceed-payment').prop('disabled', false);

    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script> 