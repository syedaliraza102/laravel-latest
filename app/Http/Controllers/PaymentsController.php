<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Hash;
use Stripe;

/**
 * The class PaymentsController extends Controller class is responsible for managing the PaymentsController Realated curd 
 * Author : Syed Ali Raza
 *
 * Created at: 04/05/2019
*/
class PaymentsController extends Controller
{   
	protected $namespace = null;
	/**
	* Function Is used to proceed_payments 
	*
	* @param \Illuminate\Http\Request $request
	* @return view of proceed_payments
	*/
	public function proceed_payments (Request $request)
	{
			try{		
			Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));		
			$customer = Stripe\Customer::create([		
				'email'=>$request->stripe_email,	
				'card'=>$request->stripeToken,		
				'description'=>"",		
			]);	
		}
		catch(Stripe\Exception\CardException $c) {	
			return back()->with('message',$c->getMessage());	
			
		}
		
		catch(Stripe\Exception\InvalidRequestException $c) {

		
			return back()->with('message',$c->getMessage());
		}					
	
		try { 	
		
			$subscription = Stripe\Subscription::create(array( 		
			"customer" => $customer->id, 	
				"items" => array( 		
					array( 					
					"plan" => $request->strip_planid, 		
					), 			
				), 		
			)); 

		}
		catch(Exception $e) { 		
			return back()->with('message',$e->getMessage());
		}
		catch(Stripe\Exception\InvalidRequestException $c) {		
			return back()->with('message',$c->getMessage());
		}
		
		
	}
	
	

}